var randombanners = {} || window.randombanners;

/**
 * Mix banner
 *
 * @param {string} element
 */
randombanners.shuffle = function (element) {
    $element = document.querySelector(element);

    if ($(element).children.length) {
        $element = document.querySelector(element);
        for (var i = $(element).children.length; i >= 0; i--) {
            $element.appendChild($element.children[Math.random() * i | 0]);
        }
    }
};

/**
 * Count click
 *
 * @param {string} bannerId
 * @param {function} callback
 */
randombanners.countClick = function (bannerId, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'index.php?eID=randombanners&banner=' + bannerId, true);
    xhr.onreadystatechange = callback(xhr);
    xhr.send();
};


(function () {
    randombanners.shuffle('.t3js-banners');

    // alert every 2 seconds
    setInterval(function () {
        randombanners.shuffle('.t3js-banners')
    }, 8000);

    var banners = document.querySelectorAll('.t3js-banner');
    banners.forEach(function (banner) {
        banner.addEventListener('click', function () {
            randombanners.countClick(banner.dataset.uid, function (request) {
                if (request.readyState !== 4 || request.status !== 200) {
                    console.log(':)')
                } else {
                    console.log(':(')
                }
            })
        })
    });
})();

