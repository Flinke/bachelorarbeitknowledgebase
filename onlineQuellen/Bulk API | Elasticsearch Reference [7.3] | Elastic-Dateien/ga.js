(function() {
  var Clearbit, providePlugin,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  providePlugin = function(pluginName, pluginConstructor) {
    var tryApply = function() {
      var ga = window[window['GoogleAnalyticsObject'] || 'ga'];

      if (typeof ga === 'function') {
        ga('provide', pluginName, pluginConstructor);
        return true;
      } else {
        return false;
      }
    }

    if (tryApply()) {
      // Native support
    } else if (window.analytics && typeof window.analytics.ready === 'function') {
      // Segment support
      analytics.ready(tryApply);
    } else {
      console.error("Clearbit error: 'ga' variable not found.");
    }
  };

  Clearbit = (function() {
      function Clearbit(tracker, config) {
        this.tracker = tracker;
        this.config = config != null ? config : {};
        this.triggerEvent = bind(this.triggerEvent, this);
        this.setIPDimensions = bind(this.setIPDimensions, this);
        this.setDimensions = bind(this.setDimensions, this);
        this.set = bind(this.set, this);
        this.done = bind(this.done, this);
        this.mapping = this.config.mapping || {};
        this.done({"ip":"93.241.19.242","domain":"tritum.de","type":"company","fuzzy":false,"company":{"id":"62b46b0f-3511-4d39-abb0-1b0228f86c64","name":"TRITUM","legalName":null,"domain":"tritum.de","domainAliases":["online-pilot.com","wt-cart.com","tritum.gmbh","hektorweb.de","i-mango.de","tugth.net","caro-line.de","tritum-media.de","creative2art.de","contrend.de","magebox.de","vwl-online.net"],"site":{"phoneNumbers":["+49 3641 7979940","+49 3641 797994"],"emailAddresses":[]},"category":{"sector":"Information Technology","industryGroup":"Software \u0026 Services","industry":"Internet Software \u0026 Services","subIndustry":"Internet","sicCode":"87","naicsCode":"54"},"tags":["Internet","B2B"],"description":"Die Onlinepiloten von TRITUM entwickeln Ihre individuelle Lösung: TYPO3 Website ✔ Magento Onlineshop ✔ Programmierung eigener Extensions ✔ Business Lösungen ✔","foundedYear":2007,"location":"Otto-Schott-Straße 13, 07745 Jena, Germany","timeZone":"Europe/Berlin","utcOffset":2,"geo":{"streetNumber":"13","streetName":"Otto-Schott-Straße","subPremise":null,"city":"Jena","postalCode":"07745","state":"Thüringen","stateCode":"TH","country":"Germany","countryCode":"DE","lat":50.9226026,"lng":11.5749151},"logo":"https://logo.clearbit.com/tritum.de","facebook":{"handle":"tritum.gmbh","likes":129},"linkedin":{"handle":"company/tritum-gmbh"},"twitter":{"handle":"tritum","id":"23780126","bio":"#TYPO3 und #Magento Agentur aus Jena • TYPO3 Gold Member • TYPO3 Business Development Partner • #TYPO3 Form Framework Team • Es twittert vorrangig Björn Jacob","followers":597,"following":921,"location":"Jena","site":"https://t.co/7haKo3blhj","avatar":"https://pbs.twimg.com/profile_images/444126099706900480/XK--qCMI_normal.png"},"crunchbase":{"handle":null},"emailProvider":false,"type":"private","ticker":null,"identifiers":{"usEIN":null},"phone":null,"metrics":{"alexaUsRank":null,"alexaGlobalRank":12135337,"employees":10,"employeesRange":"1-10","marketCap":null,"raised":null,"annualRevenue":null,"estimatedAnnualRevenue":"$1M-$10M","fiscalYearEnd":null},"indexedAt":"2019-07-12T21:37:53.017Z","tech":["google_analytics","apache","google_maps","google_tag_manager"],"parent":{"domain":null},"ultimate_parent":{"domain":null}},"geoIP":{"city":"","state":null,"stateCode":null,"country":"Germany","countryCode":"DE"}});
      }
      Clearbit.prototype.done = function(response) {
          if (response) {
             this.setIPDimensions(response);
             if (response.company){
                 this.setDimensions(response.company);
            }
            return this.triggerEvent();
         }
       };
        Clearbit.prototype.set = function(key, value) {
         if (key && value) {
           return this.tracker.set(key, value);
         }
       };
        Clearbit.prototype.setIPDimensions = function(response) {
         if (typeof response.type !== 'undefined') {
           this.set(this.mapping.type, response.type)
         }
       }

    Clearbit.prototype.setDimensions = function(company) {
      var ref, ref1;
      this.set(this.mapping.companyName, company.name);
      this.set(this.mapping.companyDomain, company.domain);
      this.set(this.mapping.companyType, company.type);
      this.set(this.mapping.companyTags, (ref = company.tags) != null ? ref.join(',') : void 0);
      this.set(this.mapping.companyTech, (ref1 = company.tech) != null ? ref1.join(',') : void 0);
      this.set(this.mapping.companySector, company.category.sector);
      this.set(this.mapping.companyIndustryGroup, company.category.industryGroup);
      this.set(this.mapping.companyIndustry, company.category.industry);
      this.set(this.mapping.companySubIndustry, company.category.subIndustry);
      this.set(this.mapping.companySicCode, company.category.sicCode);
      this.set(this.mapping.companyNaicsCode, company.category.naicsCode);
      this.set(this.mapping.companyCountry, company.geo.countryCode);
      this.set(this.mapping.companyState, company.geo.stateCode);
      this.set(this.mapping.companyCity, company.geo.city);
      this.set(this.mapping.companyFunding, company.metrics.raised);
      this.set(this.mapping.companyEstimatedAnnualRevenue, company.metrics.estimatedAnnualRevenue);
      this.set(this.mapping.companyEmployeesRange, company.metrics.employeesRange);
      this.set(this.mapping.companyEmployees, company.metrics.employees);
      return this.set(this.mapping.companyAlexaRank, company.metrics.alexaGlobalRank);
    };

    Clearbit.prototype.triggerEvent = function() {
      return this.tracker.send(
        'event',
        'Clearbit',
        'Enriched',
        'Clearbit Enriched',
        {nonInteraction: true}
      );
    };

    return Clearbit;

  })();

  providePlugin('Clearbit', Clearbit);

  

  

}).call(this);
