$(document).ready(function () {
  var tagBlogArchiveClass = ".blog-page .blog-tag-item a";
  var tagBlogSingleClass = ".blog-single .blog-tag-item a";
  var blogAuthorArchiveClass = ".blog-page a.author-name";
  var blogAuthorSingleClass = ".blog-single a.author-name";
  var blogContactUsID = "#blog-contact-us";
  var blogForumId = "#blog-forum";
  var graphStoryId = "#share-graph-story";
  var blogTitleClasss = ".blog-page .blog-title";
  var blogReadMoreClick = ".blog-read-more";

  $(tagBlogArchiveClass).click(function (e) {
    sendEvent('Blog-Archive', 'tag-click', $(this).html());
  });

  $(tagBlogSingleClass).click(function (e) {
    e.preventDefault();
    sendEvent("Blog-Single", "tag-click", $(this).html());
  });

  $(blogAuthorArchiveClass).click(function (e) {
    sendEvent("Blog-Archive", "author-click", $(this).html());
  });

  $(blogAuthorSingleClass).click(function (e) {
    sendEvent("Blog-Single", "author-click", $(this).html());
  });

  $(blogContactUsID).click(function (e) {
    sendEvent("Blog", "sidebar-click", ($(this).html()).replace(/<[^>]*>?/gm, "").trim());
  });

  $(blogForumId).click(function (e) {
    sendEvent("Blog", "sidebar-click", ($(this).html()).replace(/<[^>]*>?/gm, "").trim());
  });

  $(graphStoryId).click(function (e) {
    sendEvent("Blog", "sidebar-click", ($(this).html()).replace(/<[^>]*>?/gm, "").trim());
  });

  $(blogTitleClasss).click(function (e) {
    sendEvent('Blog', 'title-click', $(this).html());
  });

  $(blogReadMoreClick).click(function (e) {
    sendEvent('Blog', 'read-more-click', $(this).parent().parent().parent().find('.blog-title').html());
  })

  var sendEvent = function (eventCategory, eventAction, eventLabel) {
    ga('send', 'event', { eventCategory, eventAction, eventLabel, transport: 'beacon' });
  }
});
