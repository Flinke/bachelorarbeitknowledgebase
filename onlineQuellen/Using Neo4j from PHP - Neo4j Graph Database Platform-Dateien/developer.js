function expandMenu(domElement) {
    $( domElement ).toggleClass("active");
    var elem = $( domElement ).next('.panel-dev-nav');
    
    elem.toggleClass("active");
    if (elem.css('display') == 'block') {
        elem.hide();
    } else {
        elem.show();
    }
    if (elem.css('display') == 'block') {
        elem.css('display', 'none');
    } else {
        elem.css('display', 'block');
    }
    
    if (elem.css('max-height') != '0px') {
        elem.css('max-height', '0px');
    } else {
        elem.css('max-height', elem.prop('scrollHeight')+'px');
    }
}

//highlight right nav during scroll
var visDict = {};
var handleIntersect = function(entries, observer) {
  entries.forEach(function(entry) {

    visDict[ entry.target.id ] = entry.intersectionRatio;
    
    var sortable = [];
    for (var vis in visDict) {
        sortable.push([vis, visDict[vis]]);
    }
      
    sortable.sort(function(a, b) {
        return b[1] - a[1];
    });

    var mostVisibleDivId = sortable[0][0];
    $('.sectlevel1').find('li').removeClass("active");
    $('.sectlevel1').find('[href^="#' + mostVisibleDivId + '"]').parent().addClass("active");
  });
}

var setObserver = function () {
  var observer;
  var sections = document.querySelectorAll(".sect1");
  
  var options = {
    root: null,
    rootMargin: "0px",
    threshold: [0.25, 0.5, 0.6, 0.8, 0.9, 1.0]
  };
  
  observer = new IntersectionObserver(handleIntersect, options);
  sections.forEach(section => {
    observer.observe(section);
  });
}

jQuery(document).ready(function() {
  CodeMirror.colorize();
  setObserver();
  $('.dev-menu-nav').find('[href="' + window.location.pathname + '"]').parent().parent().parent().prev().click();
    
  //add sublinks to h2 and h3 elements in content of guide
  $('.guide-section-content').find('h2,h3').css('display', 'inline').after($('<i class="fa fa-link" aria-hidden="true" style="margin-left: 20px; color: lightgrey">').wrap( $('<a>') ).parent() ).next().attr('href', function () { return $(this).prev().children().first().attr('href') } );
});