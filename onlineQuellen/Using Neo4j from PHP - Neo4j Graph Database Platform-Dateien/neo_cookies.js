// Parse the URL
function getParameterByName(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

// Store URL parameter utm and track values
neo_utm_params = [];
neo_utm_params["source"] = getParameterByName('utm_source');
neo_utm_params["campaign"] = getParameterByName('utm_campaign');
neo_utm_params["medium"] = getParameterByName('utm_medium');
neo_utm_params["term"] = getParameterByName('utm_term');
neo_utm_params["placement"] = getParameterByName('utm_placement');
neo_utm_params["adgroup"] = getParameterByName('utm_adgroup');

// Get and set cookie values
neo_utm_cookies = [];
neo_utm_cookies["source"] = Cookies.get('neo_utm_source');
neo_utm_cookies["campaign"] = Cookies.get('neo_utm_campaign');
neo_utm_cookies["medium"] = Cookies.get('neo_utm_medium');
neo_utm_cookies["term"] = Cookies.get('neo_utm_term');
neo_utm_cookies["placement"] = Cookies.get('neo_utm_placement');
neo_utm_cookies["adgroup"] = Cookies.get('neo_utm_adgroup');

// Set the cookies
for (var utm_val in neo_utm_params) {
  if (neo_utm_cookies[utm_val] === undefined && neo_utm_params[utm_val] != '') {
    // Set domain to 'neo4jdotcom' when testing locally
    Cookies.set('neo_utm_' + utm_val, neo_utm_params[utm_val], { expires: 30, domain: 'neo4j.com' });
  }
}
