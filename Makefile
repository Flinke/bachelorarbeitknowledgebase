CC1=pdflatex

PDF=Bachelorarbeit_Falko_Linke_G160146PI.pdf

${PDF} : main.tex *.tex src/*.tex literatur.bib glossdata0.txt
	@${CC1} $<
	@makeglossaries $(subst .tex,.glo,$<)
	@makeglossaries $(subst .tex,.acn,$<)
	@${CC1} $<
	@makeglossaries $(subst .tex,.glo,$<)
	@makeglossaries $(subst .tex,.acn,$<)
	biber $(subst .tex,.bcf,$<)
	@${CC1} $<
	@${CC1} $<
	${CC1} $<
	@if [ -d ./FertigePDF/ ]; then echo "exist"; else mkdir ./FertigePDF; fi
	@mv -v ./$(subst .tex,.pdf,$<) ./FertigePDF/$@
	@touch -r ./FertigePDF/$@ ./$@

clean :
	rm -vf *.dvi ${PDF} *.ps *.aux *.lof *.toc *.lot *.log *.bbl *.bcf *.blg *.glg *.glo *.gls *.ist *.acn *.acr *.alg *.run.xml *.lol
	rm -vf src/*.dvi src/*.ps src/*.aux src/*.lof src/*.toc src/*.lot src/*.log src/*.bbl src/*.bcf src/*.blg src/*.glg src/*.glo src/*.gls src/*.ist src/*.acn src/*.acr src/*.alg src/*.run.xml src/*.lol

